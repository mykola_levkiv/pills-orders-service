'use strict';

import jwt from 'jsonwebtoken';

/**
 * Authorization layer
 * @param {UserModel} userModel user model
 * @returns {Authorization} authorization layer
 */
export default function authorization(userModel) {
  return new Authorization(userModel);
}
authorization.$inject = ['userModel'];

/**
 * Authorization layer constructor
 */
export class Authorization {
  
  /**
   * Authorization layer constructor
   * @param {UserModel} userModel user model
   */
  constructor(userModel) {
    this._userModel = userModel;
  }

  /**
   * Returns an middleware for user authorization
   * @returns {Function(req, res, next)} an middleware for user authorization
   */
  get requireAuthorization() {
    return this._requireAuthorization.bind(this);
  }

  async _requireAuthorization(req, res, next) {
    try {
      const token = req.headers['auth-token'];

      const decoded = jwt.decode(token, 'secret');
      const user = await this._userModel.User.getUser(decoded);

      if (!user) {
        return res.status(401).send('Authorization failed');
      }
      req.user = decoded;
      next();

    } catch (err) {
      console.log('err', err);
      next(err);
    }
  }
}
