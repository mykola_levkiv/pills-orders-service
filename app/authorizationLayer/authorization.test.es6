'use strict';

import should from 'should';
import { Service } from '../service';
import jwt from 'jsonwebtoken';

const config = {
    db: {
      name: 'test_database',
      username: 'test_user',
      password: 'qwerty'
    },
    port: 3130
  },
  app = new Service();
let userModel,
  userToken1,
  wrongToken,
  authorization;

/**
 * @test {Authorization}
 */
describe('Authorization', () => {
  before(() => {
    app.start(config);
    userModel = app._container.get('userModel');
    authorization = app._container.get('authorization');
  });

  beforeEach(async () => {
    /*await userModel.User.destroy({ where: {}, truncate: true });*/

    const userInfo = {
      login: 'userLogin1',
      password: 'userPassword',
      role: 'Pharmacist'
    };/*
    await userModel.User.create(userInfo);*/
    userToken1 = jwt.sign(userInfo, 'secret', { expiresIn: '7d' });
  });

  after(async () => {
    await app.stop();
  });

  /**
   * @test {Authorization#requireAuthorization}
   */
  it('should throw error if user provides wrong token', async (done) => {
    await authorization.requireAuthorization(
      { headers: {'auth-token': wrongToken} },
      null,
      () => done()
    );
  });

  /**
   * @test {Authorization#requireAuthorization}
   */
  it('should authorize user with correct token', async () => {
    await authorization._requireAuthorization(
      { headers: {'auth-token': userToken1} },
      null,
      Promise.resolve
    );
  });

});
