'use strict';

import Sequelize from 'sequelize';
const Op = Sequelize.Op;

/**
 * Order model
 * @param {Database} database database instance
 * @returns {OrderModel} order model instance
 */
export default function orderModel(database) {
  return new OrderModel(database);
}
orderModel.$inject = ['database'];

/**
 * Order model constructor
 */
export class OrderModel {
  
  /**
   * Order model constructor
   * @param {Database} database database instance
   */
  constructor(database) {
    const orderFields = {
      orderList: [{
        commercialName: {type: Sequelize.STRING, allowNull: false},
        quantity: {type: Sequelize.INTEGER, allowNull: false},
        pricePerItem: {type: Sequelize.FLOAT, allowNull: false}
      }],
      status: {
        type: Sequelize.STRING,
        validate: {isIn: ['pending', 'in progress', 'done', 'payed']},
        defaultValue: 'pending',
        allowNull: false
      },
      totalPrice: {type: Sequelize.FLOAT, allowNull: false}
    };

    this._Order = database.define('order', orderFields, {
      getterMethods: {
        month() {
          const months = [
            'january', 'february', 'march', 'april',
            'may', 'june', 'july', 'august',
            'september', 'october', 'november', 'december'
          ];
          const monthNum = new Date(this.updatedAt).getMonth();
          return months[monthNum];
        }
      }
    });

    /**
     * Updates status of the order
     * @param {String} id order id
     * @param {String} status new order status
     * @returns {Promise} promise which will be resolved when order status will be updated
     */
    this._Order.updateStatus = (id, status) => {
      return this._Order.update( {status}, {where: {id}} );
    };

    /**
     * Creates new order
     * @param {Array<Object>} orderList list of ordered items
     * @returns {Promise} promise which will be resolved when new order will be created
     */
    this._Order.createOrder = (orderList) => {
      const totalPrice = orderList.reduce( (price, item) => price + item.pricePerItem, 0 );
      return this._Order.create({ orderList, totalPrice });
    };

    /**
     * Finds all orders by status
     * @param {String} status order status
     * @returns {Promise} promise which will be resolved when orders will be retreived
     */
    this._Order.findOrdersByStatus = (status) => {
      return this._Order.findAll({ where: {status} });
    };

    /**
     * Finds order by unique id
     * @param {Number} id order id
     * @returns {Promise} promise which will be resolved when order will be retreived
     */
    this._Order.findOrderById = (id) => {
      return this._Order.findById(id);
    };

    /**
     * Finds done orders by specific month
     * @param {String} month month full name
     * @returns {Promise} promise which will be resolved when orders will be retreived
     */
    this._Order.calculateIncomeByMonth = (month) => {
      return this._Order.sum('totalPrice', { where: { status: 'payed', month } });
    };

  }

  get Order() {
    return this._Order;
  }
}
