'use strict';

import Sequelize from 'sequelize';
const Op = Sequelize.Op;

/**
 * User model
 * @param {Database} database database instance
 * @returns {UserModel} user model instance
 */
export default function userModel(database) {
  return new UserModel(database);
}
userModel.$inject = ['database'];

/**
 * User model constructor
 */
export class UserModel {
  
  /**
   * User model constructor
   * @param {Database} database database instance
   */
  constructor(database) {
    const userFields = {
      login: {type: Sequelize.STRING, allowNull: false},
      password: {type: Sequelize.STRING, allowNull: false},
      role: {
        type: Sequelize.STRING,
        validate: {isIn: ['Pharmacist', 'Accountant', 'Cashier']},
        allowNull: false
      }
    };

    this._User = database.define('user', userFields);

    this._User.sync({force: true});

    /**
     * Creates new user
     * @param {String} login user's login
     * @param {String} password user's password
     * @param {String} role user's role
     * @returns {Promise} promise which will be resolved when new user will be created
     */
    this._User.createUser = (login, password, role) => {
      return this._User.create({ login, password, role });
    };

    /**
     * Get user
     * @param {String} login user's login
     * @param {String} password user's password
     * @param {String} role user's role
     * @returns {Promise} promise which will be resolved when user will be retreived
     */
    this._User.getUser = ({ login, password, role }) => {
      return this._User.findOne({ where: { login, password, role } });
    };

    /**
     * Checks if user with specified login is already exists
     * @param {String} login new login
     * @returns {Promise} promise which will be resolved when new login will be checked
     */
    this._User.checkLogin = (login) => {
      return this._User.findOne({ where: {login} });
    }

  }

  get User() {
    return this._User;
  }
}
