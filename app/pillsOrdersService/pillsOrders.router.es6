'use strict';

import express from 'express';
import validator from 'node-validator';

/**
 * Pills orders REST API router service
 * @param {Authorization} authorization authorization layer
 * @param {PillsOrdersService} pillsOrdersService pills orders service
 * @returns {PillsOrdersRouter} Pills orders REST API router service
 */
export default function pillsOrdersRouter(authorization, pillsOrdersService) {
  return new PillsOrdersRouter(authorization, pillsOrdersService);
}
pillsOrdersRouter.$inject = ['authorization', 'pillsOrdersService'];

/**
 * Pills orders router service
 */
export class PillsOrdersRouter {

  /**
   * Constructs pills orders router instance
   * @param {Authorization} authorization autorization layer
   * @param {PillsOrdersService} pillsOrdersService pills orders service
   */
  constructor(authorization, pillsOrdersService) {
    this._authorization = authorization;
    this._pillsOrdersService = pillsOrdersService;
  }

  pillsOrdersApi() {
    let router = express.Router();

    const signInRules = validator.isObject()
      .withRequired('login', validator.isString())
      .withRequired('password', validator.isString());
    const signUpRules = signInRules
      .withRequired('role', validator.isString({regex: /(Pharmacist)|(Accountant)|(Cashier)/}));
    const updateStatusRules = validator.isObject()
      .withRequired('status', validator.isString({regex: /(in progres)|(done)|(payed)/}));
    const newOrderRules = validator.isArray(
      validator.isObject()
        .withRequired('commercialName', validator.isString())
        .withRequired('quantity', validator.isInteger())
        .withRequired('pricePerItem', validator.isNumber())
    );

    router.route('/sign-up')
      .post(validator.express(signUpRules), this._pillsOrdersService.signUp);
    
    router.route('/sign-in')
      .post(validator.express(signInRules), this._pillsOrdersService.signIn);

    router.route('/orders')
      .post(
        this._authorization.requireAuthorization,
        validator.express(newOrderRules),
        this._pillsOrdersService.createOrder
      );

    router.route('/orders/statuses/:status')
      .get(this._authorization.requireAuthorization, this._pillsOrdersService.getOrdersByStatus);

    router.route('/orders/:orderId/status')
      .put(
        this._authorization.requireAuthorization,
        validator.express(updateStatusRules),
        this._pillsOrdersService.updateOrderStatus
      );

    router.route('/orders/:orderId')
      .get(this._authorization.requireAuthorization, this._pillsOrdersService.getOrderById);

    router.route('/reports/months/:month')
      .get(this._authorization.requireAuthorization, this._pillsOrdersService.generateReportForMonth);

    return router;
  }
}
