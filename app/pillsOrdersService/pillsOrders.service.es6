'use strict';

import jwt from 'jsonwebtoken';

/**
 * Pills orders
 * @param {UserModel} userModel user model instance
 * @param {OrderModel} orderModel order model instance
 * @returns {PillsOrdersService} pills orders service instance
 */
export default function pillsOrdersService(userModel, orderModel) {
  return new PillsOrdersService(userModel, orderModel);
}
pillsOrdersService.$inject = ['userModel', 'orderModel'];

/**
 * Pills orders service constructor
 */
export class PillsOrdersService {
  
  /**
   * Constructs pills orders service instance
   * @param {UserModel} userModel user model instance
   * @param {OrderModel} orderModel order model instance
   */
  constructor (userModel, orderModel) {
    this._userModel = userModel;
    this._orderModel = orderModel;
  }

  /**
   * Returns an endpoint for new user's creation
   * @returns {Function(req, res, next)} an endpoint for new user's creation
   */
  get signUp() {
    return this._signUp.bind(this);
  }

  /**
   * Returns an endpoint for user's authorization
   * @returns {Function(req, res, next)} an endpoint for user's authorization
   */
  get signIn() {
    return this._signIn.bind(this);
  }

  /**
   * Returns an endpoint for new order creation
   * @returns {Function(req, res, next)} an endpoint for new order creation
   */
  get createOrder() {
    return this._createOrder.bind(this);
  }

  /**
   * Returns an endpoint for orders retreival by status name
   * @returns {Function(req, res, next)} an endpoint for orders retreival by status name
   */
  get getOrdersByStatus() {
    return this._getOrdersByStatus.bind(this);
  }

  /**
   * Returns an endpoint for order retreival by id
   * @returns {Function(req, res, next)} an endpoint for order retreival by id
   */
  get getOrderById() {
    return this._getOrderById.bind(this);
  }

  /**
   * Returns an endpoint for order status updating
   * @returns {Function(req, res, next)} an endpoint for order status updating
   */
  get updateOrderStatus() {
    return this._updateOrderStatus.bind(this);
  }

  /**
   * Returns an endpoint for month report retreival
   * @returns {Function(req, res, next)} an endpoint for month report retreival
   */
  get generateReportForMonth() {
    return this._generateReportForMonth.bind(this);
  }

  async _signUp(req, res, next) {
    try {
      const { login, password, role } = req.body;

      const user = await this._userModel.User.checkLogin(login);
      if (user) {
        return res.status(403).send('User with login ' + login + ' is already exist');
      }

      await this._userModel.User.createUser(login, password, role);
      const token = jwt.sign({ login, password, role }, 'secret', { expiresIn: '7d' });
      
      res.status(201).json({ token });
    
    } catch (err) {
      next(err);
    }
  }

  async _signIn(req, res, next) {
    try {
      const { login, password } = req.body;
      
      const loginExist = await this._userModel.User.checkLogin(login);
      if (!loginExist) {
        return res.status(404).send('User with such login does not exist');
      }

      const user = await this._userModel.User.getUser(login, password);
      if (!user) {
        return res.status(403).send('Password is incorrect. Please try again');
      }

      const token = jwt.sign({ login, password, role: user.role }, 'secret', { expiresIn: '7d' });
      res.status(201).json({ token });
    
    } catch (err) {
      next(err);
    }
  }

  async _createOrder(req, res, next) {
    try {
      const { role } = req.user;
      const orderList = req.body;

      if (role !== 'Cashier') {
        return res.status(403).send('User with role ' + role + ' can not create new order');
      }

      const newOrder = await this._orderModel.Order.createOrder(orderList);
      res.status(201).send({ id: newOrder.id });

    } catch (err) {
      next(err);
    }
  }

  async _getOrdersByStatus(req, res, next) {
    try {
      const { role } = req.user;
      const status = req.params.status;

      if (['Pharmacist', 'Accountant'].indexOf(role) === -1) {
        return res.status(403).send('User with role ' + role + ' can not retreive orders by status');
      } else if ( ['pending', 'in progress'].indexOf(status) === -1 && role !== 'Pharmacist' ) {
        return res.status(403).send('user with role ' + role + ' can not retreive orders with status '
          + status);
      }

      const orders = await this._orderModel.Order.findOrdersByStatus(status);
      res.status(200).send(orders.get({ plain: true }));

    } catch (err) {
      next(err);
    }
  }

  async _getOrderById(req, res, next) {
    try {
      const { role } = req.user;
      const id = req.params.orderId;

      if (['Cashier', 'Accountant'].indexOf(role) === -1) {
        return res.status(403).send('User with role ' + role + ' can not retreive orders by id');
      }

      const order = await this._orderModel.Order.findOrderById(id);
      if (!order) {
        return res.status(404).send('Order with order id ' + id + ' was not found');
      }

      res.status(200).send(order.get({ plain: true }));

    } catch (err) {
      next(err);
    }
  }

  async _updateOrderStatus(req, res, next) {
    try {
      const { role } = req.user;
      const id = req.params.orderId;
      const { status } = req.body;

      if (
        (role !== 'Cashier' && status !== 'payed')
        || (role !== 'Pharmacist' && ['in progress', 'done'].indexOf(status) !== -1)
      ) {
        return res.status(403).send('User with role ' + role + ' can not update status to ' + status);
      }

      await this._orderModel.Order.updateStatus(id, status);
      res.status(204).send();

    } catch (err) {
      next(err);
    }
  }

  async _generateReportForMonth(req, res, next) {
    const { role } = req.user;
    const month = req.params.month;

    if (role !== 'Accountant') {
      return res.status(403).send('User with role ' + role + ' can not retreive monthly reports');
    }

    const income = await this._orderModel.Order.calculateIncomeByMonth(month);

    res.status(200).send({ month, income });
  }

}
