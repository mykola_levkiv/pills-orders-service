'use strict';

import should from 'should';
import { Service } from '../service';
import sinon from 'sinon';
import request from 'supertest-promised';
import jwt from 'jsonwebtoken';

const config = {
    db: {
      name: 'test_database',
      username: 'test_user',
      password: 'qwerty'
    },
    port: 3130
  },
  app = new Service();
const cashierInfo = {
    login: 'cashierLogin',
    password: 'cashierPassword',
    role: 'Cashier'
  },
  accountantInfo = {
    login: 'accountantLogin',
    password: 'accountantPassword',
    role: 'Accountant'
  },
  pharmacistInfo = {
    login: 'pharmacistLogin',
    password: 'pharmacistPassword',
    role: 'Pharmacist'
  };
const orderList = [{
  commercialName: 'coolPills',
  quantity: 3,
  pricePerItem: 3.30
}];
let server,
  userModel,
  orderModel,
  authorization,
  sandbox;

/**
 * @test {PillsOrdersService}
 */
describe('PillsOrdersService', () => {
  before(() => {
    app.start(config);
    server = app._app;
    userModel = app._container.get('userModel');
    orderModel = app._container.get('orderModel');
    authorization = app._container.get('authorization');
    sandbox = sinon.sandbox.create();
  });

  beforeEach(async () => {/*
    await userModel.User.destroy({ where: {}, truncate: true });
    await orderModel.Order.destroy({ where: {}, truncate: true });
    
    /*await userModel.User.create(cashierInfo);
    await userModel.User.create(accountantInfo);
    await userModel.User.create(pharmacistInfo);*/
  });

  afterEach(() => {
    sandbox.restore();
  })

  after(async () => {
    await app.stop();
  });

  /**
   * @test {PillsOrdersService#signUp}
   */
  it('should return 403 if user with such login already exists', async () => {
    await request(server)
      .post('/sign-up')
      .set('Accept', 'application/json')
      .send(cashierInfo)
      .expect(403)
      .end();
  });

  /**
   * @test {PillsOrdersService#signUp}
   */
  it('should create new user', async () => {
    await request(server)
      .post('/sign-up')
      .set('Accept', 'application/json')
      .send({
        login: 'uniqueLogin',
        password: 'password',
        role: 'Cashier'
      })
      .expect(201)
      .end();

    const newUser = await userModel.User.checkLogin('uniqueLogin');
    should.exist(newUser);
  });

  /**
   * @test {PillsOrdersService#signIn}
   */
  it('should return 404 if user with such login does not exist', async () => {
    await request(server)
      .post('/sign-in')
      .set('Accept', 'application/json')
      .send({
        login: 'wrongLogin',
        password: 'password',
        role: 'Cashier'
      })
      .expect(404)
      .end();
  });

  /**
   * @test {PillsOrdersService#signIn}
   */
  it('should return 403 if user has incorrect password', async () => {
    await request(server)
      .post('/sign-in')
      .set('Accept', 'application/json')
      .send(Object.assign(cashierInfo, { password: 'wrongPassword' }))
      .expect(403)
      .end();
  });

  /**
   * @test {PillsOrdersService#signIn}
   */
  it('should create authorization token', async () => {
    const response = await request(server)
      .post('/sign-in')
      .set('Accept', 'application/json')
      .send(cashierInfo)
      .expect(201)
      .end()
      .get('body');

    should.exist(response.token);
  });

  /**
   * @test {PillsOrdersService#createOrder}
   */
  it('should return 400 if order validation failed', async () => {
    sandbox.stub(authorization, '_requireAuthorization').callsFake((req, res, next) => next());

    await request(server)
      .post('/orders')
      .set('Accept', 'application/json')
      .send([{
        commercialName: 'pills'
      }])
      .expect(400)
      .end();
  });

  /**
   * @test {PillsOrdersService#createOrder}
   */
  it('should return 403 if user role is not Cashier', async () => {
    sandbox.stub(authorization, '_requireAuthorization').callsFake((req, res, next) => {
      req.user = accountantInfo;
      next();
    });

    await request(server)
      .post('/orders')
      .set('Accept', 'application/json')
      .send([{
        commercialName: 'pills',
        quantity: 1,
        pricePerItem: 2.50
      }])
      .expect(403)
      .end();
  });

  /**
   * @test {PillsOrdersService#createOrder}
   */
  it('should create new order', async () => {
    const order = [{
      commercialName: 'pillsName',
      quantity: 3,
      pricePerItem: 3.30
    }];
    sandbox.stub(authorization, '_requireAuthorization').callsFake((req, res, next) => {
      req.user = cashierInfo;
      next();
    });

    await request(server)
      .post('/orders')
      .set('Accept', 'application/json')
      .send(order)
      .expect(201)
      .end();

    const [orderDoc] = await orderModel.Order.findOrdersByStatus('pending');
    orderDoc.commercialName.should.be.eql(order.commercialName)
  });

  /**
   * @test {PillsOrdersService#getOrdersByStatus}
   */
  it('should return 403 if user role is not Pharmacist or Accountant', async () => {
    sandbox.stub(authorization, '_requireAuthorization').callsFake((req, res, next) => {
      req.user = cashierInfo;
      next();
    });

    await request(server)
      .get('/orders/statuses/pending')
      .set('Accept', 'application/json')
      .expect(403)
      .end();
  });

  /**
   * @test {PillsOrdersService#getOrdersByStatus}
   */
  it('should return 403 if Pharmacist is trying to retreive orders with status payed', async () => {
    sandbox.stub(authorization, '_requireAuthorization').callsFake((req, res, next) => {
      req.user = pharmacistInfo;
      next();
    });

    await request(server)
      .get('/orders/statuses/payed')
      .set('Accept', 'application/json')
      .expect(403)
      .end();
  });

  /**
   * @test {PillsOrdersService#getOrdersByStatus}
   */
  it('should return orders with specified status', async () => {
    sandbox.stub(authorization, '_requireAuthorization').callsFake((req, res, next) => {
      req.user = pharmacistInfo;
      next();
    });
    orderModel.Order.create({
      orderList,
      status: 'in progress'
    });

    const response = await request(server)
      .get(`/orders/statuses/${encodeURIComponent('in progress')}`)
      .set('Accept', 'application/json')
      .expect(201)
      .end()
      .get('body');

    response[0].status.should.be.eql('in progress');
  });

  /**
   * @test {PillsOrdersService#updateOrderStatus}
   */
  it('should return 400 if status validation failed', async () => {
    sandbox.stub(authorization, '_requireAuthorization').callsFake((req, res, next) => next());

    await request(server)
      .put(`/orders/orderId/status`)
      .set('Accept', 'application/json')
      .send({ status: 'wrongStatus' })
      .expect(400)
      .end();
  });

  /**
   * @test {PillsOrdersService#updateOrderStatus}
   */
  it('should return 403 if user role is Accountant', async () => {
    sandbox.stub(authorization, '_requireAuthorization').callsFake((req, res, next) => {
      req.user = accountantInfo;
      next();
    });

    await request(server)
      .put('/orders/orderId/status')
      .set('Accept', 'application/json')
      .send({ status: 'payed' })
      .expect(403)
      .end();
  });
  
  /**
   * @test {PillsOrdersService#updateOrderStatus}
   */
  it('should return 404 if order with specified id was not found', async () => {
    sandbox.stub(authorization, '_requireAuthorization').callsFake((req, res, next) => {
      req.user = pharmacistInfo;
      next();
    });

    await request(server)
      .put('/orders/wrongOrderId/status')
      .set('Accept', 'application/json')
      .send({ status: 'done' })
      .expect(404)
      .end();
  });

  /**
   * @test {PillsOrdersService#updateOrderStatus}
   */
  it('should update status for existing order', async () => {
    orderModel.Order.create({
      id: 1,
      orderList,
      status: 'done'
    });
    sandbox.stub(authorization, '_requireAuthorization').callsFake((req, res, next) => {
      req.user = cashierInfo;
      next();
    });

    await request(server)
      .put('/orders/1/status')
      .set('Accept', 'application/json')
      .send({ status: 'payed' })
      .expect(204)
      .end();

    const order = orderModel.Order.findById(1);
    order.status.should.be.eql('payed');
  });

  /**
   * @test {PillsOrdersService#getOrderById}
   */
  it('should return 404 if order was not found', async () => {
    sandbox.stub(authorization, '_requireAuthorization').callsFake((req, res, next) => {
      req.user = cashierInfo;
      next();
    });

    await request(server)
      .get('/orders/wrongOrderId')
      .set('Accept', 'application/json')
      .expect(404)
      .end();
  });

  /**
   * @test {PillsOrdersService#getOrderById}
   */
  it('should return order with specified id', async () => {
    orderModel.Order.create({
      id: 1,
      orderList,
      status: 'done'
    });
    sandbox.stub(authorization, '_requireAuthorization').callsFake((req, res, next) => {
      req.user = cashierInfo;
      next();
    });

    const response = await request(server)
      .get('/orders/1')
      .set('Accept', 'application/json')
      .expect(200)
      .end()
      .get('body');

    response.orderList[0].commercialName.should.be.eql('coolPills');
  });

  /**
   * @test {PillsOrdersService#generateReportForMonth}
   */
  it('should return 403 if user role is not Accountant', async () => {
    sandbox.stub(authorization, '_requireAuthorization').callsFake((req, res, next) => {
      req.user = cashierInfo;
      next();
    });

    await request(server)
      .get('/reports/months/february')
      .set('Accept', 'application/json')
      .expect(403)
      .end();
  });

  /**
   * @test {PillsOrdersService#generateReportForMonth}
   */
  it('should return report for specified month', async () => {
    sandbox.stub(authorization, '_requireAuthorization').callsFake((req, res, next) => {
      req.user = accountantInfo;
      next();
    });

    orderModel.Order.createBulk([
      { orderList, totalPrice: 30, updatedAt: new Date().setMonth(1) },
      { orderList, totalPrice: 20, updatedAt: new Date().setMonth(1) },
      { orderList, totalPrice: 10, updatedAt: new Date().setMonth(2) }
    ])

    const response = await request(server)
      .get('/reports/months/february')
      .set('Accept', 'application/json')
      .expect(200)
      .end()
      .get('body');

    response.income.should.be.eql(50);
  });

});
