'use strict';

import express from 'express';
import intravenous from 'intravenous';
import Sequelize from 'sequelize';
import bodyParser from 'body-parser';
import userModel from './models/user.model';
import orderModel from './models/order.model';
import pillsOrdersService from './pillsOrdersService/pillsOrders.service';
import pillsOrdersRouter from './pillsOrdersService/pillsOrders.router';
import authorization from './authorizationLayer/authorization';

export class Service {
  constructor() {
    this._container = intravenous.create();
    this._app = express();
    this._routes = [];
  }

  start(config) {
    this.initMiddlewares();
    this.initDatabase(config);
    this.injectDependencies();
    this.registerRouters();
    this.initRoutes();

    const port = config.port || 3000;
    this._app.listen(port, () => {
      console.log('App started listening on port ' + port);
    });
  }

  stop() {
    this._app = null;
    this._routes = [];

    const dbStopPromise = this._database ? this._database.close() : Promise.resolve();

    return dbStopPromise;
  }

  injectDependencies() {
    this._container.register('userModel', userModel);
    this._container.register('orderModel', orderModel);
    this._container.register('authorization', authorization);
    this._container.register('pillsOrdersService', pillsOrdersService);
    this._container.register('pillsOrdersRouter', pillsOrdersRouter);
  }

  registerRouters() {
    this._routes.push({
      url: '/',
      provider: container => container.get('pillsOrdersRouter').pillsOrdersApi()
    });
  }

  initMiddlewares() {
    this._app.use(bodyParser.urlencoded({extended: true}));
    this._app.use(bodyParser.json({limit: '50mb'}));
  }

  initRoutes() {
    this._routes.forEach(route => {
      this._app.use(route.url, route.provider(this._container));
    });
  }

  initDatabase(config) {
    this._database = new Sequelize(config.db.name, config.db.username, config.db.password, {
      dialect: 'postgres'
    });

    this._container.register('database', this._database);
  }
}
